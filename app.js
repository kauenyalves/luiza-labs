const express = require("express")
const app = express()
const host = process.env.HOSTNAME || "localhost"
const port = process.env.PORT || 3000

const { uniqueNamesGenerator, adjectives, animals} = require
("unique-names-generator")

const appName = uniqueNamesGenerator({
  dictionaries: [adjectives, animals],
  length:2
}).toUpperCase()

app.use(express.json())

app.get("/", (req, res) => {
  res.send({message: `Olá Mundo, ${appName}!`})
})


const server = app.listen(port, () => {
  if (server) {
    const address = server.address();
    console.log(" Docker está rodando no meu servidor!", appName, port, host, address)
  } else {
    console.error('Failure upon starting server.');
  }
});


